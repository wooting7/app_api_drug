import 'package:flutter/material.dart';

class ComponentListITemRow extends StatelessWidget {
  const ComponentListITemRow({super.key,
  required this.name,
  required this.variableName});

  final String name;
  final String variableName;
  



  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text('●',style: TextStyle(fontSize: 10),),
        const SizedBox(width: 10),      //MediaQuery.of(context).size.width - 40 - 20,
        Container(
          width: MediaQuery.of(context).size.width - 240,
          child: Text(name,style: TextStyle(),),
        ),
        const SizedBox(width: 10,),
        Text(variableName, style: TextStyle(color: Colors.grey))
      ],

    );
  }
}
