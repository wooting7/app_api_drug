import 'package:app_api_test_drug/components/component_list_item_row.dart';
import 'package:app_api_test_drug/model/drug_list_item.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.item,
    required this.callback
  });

  final DrugListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(left: 10, right: 10 , bottom: 10),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
          border: Border.all(color: const Color.fromRGBO(5, 5, 5, 100)),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('${item.CPNT_CD.isEmpty ? '-' : item.CPNT_CD}',
              style: TextStyle(fontWeight: FontWeight.bold),),
            ComponentListITemRow(name: '성분명(한글):', variableName: '${item.DRUG_CPNT_KOR_NM.isEmpty ? '-' : item.DRUG_CPNT_KOR_NM}'),
            ComponentListITemRow(name: '성분명(영문):', variableName: '${item.DRUG_CPNT_ENG_NM.isEmpty ? '-' : item.DRUG_CPNT_ENG_NM}'),
            ComponentListITemRow(name: '제형명:', variableName: '${item.FOML_NM.isEmpty ? '-' : item.FOML_NM}'),
            Divider(),
            ComponentListITemRow(name: '투여단위:', variableName: '${item.DAY_MAX_DOSG_QY_UNIT.isEmpty ? '-' : item.DAY_MAX_DOSG_QY_UNIT}'),
            ComponentListITemRow(name: '1일최대투여량:', variableName: '${item.DAY_MAX_DOSG_QY == '0.00000' ? '-' : item.DAY_MAX_DOSG_QY}'),
            ComponentListITemRow(name: '1일최대투여량:', variableName: '${item.DAY_MAX_DOSG_QY == '0.00000' ? '-' : item.DAY_MAX_DOSG_QY}'),


              ],
        ),
      ),
    );
  }
}